from os import system
from sys import exit, argv
from urllib2 import Request, build_opener, HTTPHandler
from json import loads
from time import sleep
from cv2 import imread, resize

# convert RGB to XY
def convertToXY(red, green, blue):
	
	# Gamma correction
	if red > 0.04045:
		red = ((red + 0.055) / (1.0 + 0.055)) ** 2.4
	else:
		red = red / 12.92

	if green > 0.04045:
		green = ((green + 0.055) / (1.0 + 0.055))** 2.4
	else: 
		green = green / 12.92

	if blue > 0.04045:
		blue = ((blue + 0.055) / (1.0 + 0.055)) **  2.4 
	else: 
		blue = blue / 12.92

	# Wide RGB D65 conversion
	X = red * 0.649926 + green * 0.103455 + blue * 0.197109
	Y = red * 0.234327 + green * 0.743075 + blue * 0.022598
	Z = green * 0.053077 + blue * 1.035763;

	# Calculate the xy values from the XYZ values
	x = X / (X + Y + Z); 
	y = Y / (X + Y + Z);
	
	# Use the Y value of XYZ as brightness The Y value indicates the brightness of the converted color.
	return x, y, Y

def get_colors(filename):
	image = imread(filename)
	dims = image.shape
	rows = dims[0] / 25
	cols = dims[1] / 25

	image = resize(image, (cols, rows))

	red = 0
	green = 0
	blue = 0
	counter = 0

	for r in xrange(rows-1):
		for c in xrange(cols-1):
			pixel = image[r][c]
			if sum(pixel) != 0:
				blue += pixel[0]
				green += pixel[1]
				red += pixel[2]
				counter += 1

	if counter != 0:
		red = red / float(counter) / 255
		green = green / float(counter) / 255
		blue = blue / float(counter) / 255

	return [blue, green, red]

if __name__ == "__main__":
	if len(argv) < 2:
		print "Usage: python go.py <BRIGHTNESS>"
		exit()
	bri = argv[1]
	
	# get IP address of the bridge
	opener = build_opener(HTTPHandler)
	request = Request('	https://www.meethue.com/api/nupnp')
	url = opener.open(request)
	response = loads(url.read())
	ip = response[0]['internalipaddress']
	
	# set the username and the light to be adjusted
	# TODO generalize for other users
	username = 'anoukvisser'
	
	# set the light and the brightness
	light = '2'
	# bri = '60'

	# create the url for request
	urlForRequest = 'http://' + ip + '/api/' + username + '/lights/' + light + '/state' 

	while True:
		# take a screenshot
		# -x is to make it silent
		system("screencapture -x screen.png")

		colors = get_colors('screen.png')

		# remove the screenshot
		system("rm screen.png")
		
		# if the color is almost black
		if colors[2] < 0.02 and colors[1] < 0.02 and colors[0] < 0.02:
			# turn the light off.
			data = '{"bri":1}'
		else:
			# convert the colors
			x, y, Y = convertToXY(colors[2], colors[1], colors[0])
			# set the data
			data = '{"on":true, "bri":'+bri+', "xy":'+str([x, y])+', "transitiontime":3}'
		
		# create the PUT request
		request = Request(urlForRequest, data=data)
		request.get_method = lambda: 'PUT'

		# open the request
		url = opener.open(request)
